# Django CRUD

#### Simple Django CRUD Application

#### Things to do :
1. Clone this repo:
    ```bash
    https://gitlab.com/hendisantika/djangocrudapp.git
    ```
2. Go to the folder : `cd djangocrudapp`
3. Install python libs :
    ```bash
    pip install -r requirements.txt
    ```
4. Run this command
    ```
     python manage.py makemigrations
    ```
    
    ```bash
    python manage.py migrate
    ```
5. Run this Django App server
    ```bash
    python manage.py runserver
    ``` 
    
#### Screen shot

##### Home Page

![Home Page](img/home.png "Home Page")

##### Add New Book

![Add New Book](img/add1.png "Add New Book")

![Add New Book](img/add2.png "Add New Book")

##### List All Books

![List All Books](img/list1.png "List All Book")

![List All Books](img/list2.png "List All Book")

##### Edit Book

![Edit Book Data](img/edit.png "Edit Book Data")